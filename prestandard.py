"""
Check for the following:
* Line length <= 120
* No tabs (proper indenting)
* DocString/ multiline strings double quoted
* No Syntax errors (could be part of another set of checks?)
"""